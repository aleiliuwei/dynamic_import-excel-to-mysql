# 动态导入excel到mysql数据库

#### 介绍
动态的将excel导入数据库，批量导入数据接口完成(接收文件保存到服务器、表头与配置表建立对应关系、写入日志记录、创建临时表并写入数据)
支持日志记录。字段正则校验，返回成功导入条数，增加日志表的状态字段更新，成功导入数，结束时间，临时表名，动态导入增加合并单元格的识别，空行识别。增强可复用性，完善响应信息

详情请参考博客地址：https://blog.csdn.net/languageStudent/article/details/116989687

#### 软件架构
SpringBoot
MybatisPlus
Java11


#### 安装教程

1.  修改数据库配置文件
2.  初始化数据库

#### 使用说明
(执行附带的sql文件，初始化mysql数据库)
1、启动主启动类
    请求接口地址: (localhost:8080/pub-imp-excel-jd-log/uploads?menuId=1)
    如图所示![img.png](img.png)
2、接口地址在 dynamic_import-excel-to-mysql/ src / main / java / com / xjt / excel / controller/PublmpExcelJdlogController接口类
3、调用接口上传excel文件。并附带menuid.调用结束，上传成功

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
