package com.xjt.excel.mapper;

import com.xjt.excel.entity.PubImpExcelValidLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 批量导入数据校验日志信息表 Mapper 接口
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface PubImpExcelValidLogMapper extends BaseMapper<PubImpExcelValidLog> {

}
